\contentsline {chapter}{\numberline {1}Introduction to polynyas}{1}
\contentsline {section}{\numberline {1.1}Frazil ice production}{2}
\contentsline {chapter}{\numberline {2}Review of past experimental work and field measurements}{7}
\contentsline {chapter}{\numberline {3}Mathematical modelling of frazil ice}{11}
\contentsline {section}{\numberline {3.1}Introduction to modelling}{11}
\contentsline {section}{\numberline {3.2}Model components}{13}
\contentsline {section}{\numberline {3.3}Crystal growth and rise}{14}
\contentsline {subsection}{\numberline {3.3.1}Growth}{14}
\contentsline {subsection}{\numberline {3.3.2}Buoyant rise}{15}
\contentsline {subsection}{\numberline {3.3.3}Single crystal rising and growing in a fixed temperature field}{17}
\contentsline {subsection}{\numberline {3.3.4}Crystal distribution equation}{19}
\contentsline {section}{\numberline {3.4}Temperature equation}{21}
\contentsline {section}{\numberline {3.5}Boundary and initial conditions}{22}
\contentsline {section}{\numberline {3.6}Nucleation}{24}
\contentsline {section}{\numberline {3.7}Non-dimensional governing equations}{26}
\contentsline {chapter}{\numberline {4}Numerical methods}{29}
\contentsline {chapter}{\numberline {5}Results}{33}
\contentsline {section}{\numberline {5.1}Numerical solutions and comparison with experimental results}{33}
\contentsline {subsection}{\numberline {5.1.1}Clark \& Doering (2009)}{36}
\contentsline {subsection}{\numberline {5.1.2}Clark \& Doering (2004)}{40}
\contentsline {subsection}{\numberline {5.1.3}Daly \& Colbeck (1986)}{42}
\contentsline {subsection}{\numberline {5.1.4}Differences between model and experiment}{43}
\contentsline {section}{\numberline {5.2}Steady solutions without crystal diffusion}{45}
\contentsline {section}{\numberline {5.3}Vertically uniform approximation}{51}
\contentsline {subsection}{\numberline {5.3.1}Steady solution}{52}
\contentsline {subsection}{\numberline {5.3.2}Transients}{54}
\contentsline {chapter}{\numberline {6}Conclusions}{59}
\contentsline {chapter}{\numberline {A}Discussion of approximations made in the modelling, and some simple extensions}{63}
\contentsline {section}{\numberline {A.1}Crystal growth}{63}
\contentsline {section}{\numberline {A.2}Buoyant rise}{66}
\contentsline {subsection}{\numberline {A.2.1}Maximum crystal size with alternative models for rise velocity}{68}
