Ncruns = 6;
NZruns = 6;
ctarr = [  1.14062492   1.48398179   1.93069773   2.51188643   3.26802759   4.25178630 ];
Zmarr = [  1.14062492   1.48398179   1.93069773   2.51188643   3.26802759   4.25178630 ];
dS = 0.004000;
Sm = 4.000000;
filebase = '/data/septal/pwf25/czruns/july14/julrun22';
Thbs=zeros(NZruns,Ncruns);
Thbm=zeros(NZruns,Ncruns);
TatThbm=zeros(NZruns,Ncruns);
for ic = 1:Ncruns
    ct=ctarr(ic);
    for iZ = 1:NZruns
	tic;
	Zm=Zmarr(iZ);
	if Zm>1
       dZ = min(0.5,Zm/19);
    else
       dZ = max(0.005,Zm/19);
    end
	if(ct>1)
	  Tm=ct^0.3*80*Zm^0.7;
	else
         Tm=20;
	end
        dTpossibilities=[0.2*dZ^2, 0.2*ct*dZ/Sm, 0.1*ct*dS/1.4,0.001]
	dT=0.5*min(dTpossibilities);
        freq=ceil(1000/Tm)
	Pstruct=Junefull(ct,Zm,dT,Tm,dS,Sm,dZ,freq);
	Pstruct.ct=ct;
	TN=length(Pstruct.T);
	ZN=length(Pstruct.Z);
	SN=length(Pstruct.S);
       Pbar=zeros(SN,TN);
        for s = 1:SN
            for t=1:TN
		Pbar(s,t)=Pstruct.dZ*mysum(Pstruct.P(s,:,t));
            end
        end
        Sbarmax=zeros(TN,1);
        for t = 1:TN
            [~,I]=max(Pstruct.S'.^0.5.*Pbar(:,t));
            Sbarmax(t)=Pstruct.S(I);
        end
        timeseries(iZ,ic).Sbarmax=Sbarmax;
        timeseries(iZ,ic).T=Pstruct.T;
        Thbar=zeros(1,TN);
        for t=1:TN
            Thbar(t)=mysum(Pstruct.Th(:,t))*Pstruct.dZ;
        end
        timeseries(iZ,ic).Thbar=Thbar;
	[Thbm(iZ,ic),I]=max(Thbar);
        TatThbm(iZ,ic)=Pstruct.T(I);
        Thbs(iZ,ic)=Thbar(end);
	Zseries(iZ,ic).Thsteady=Pstruct.Th(:,end);
        Zseries(iZ,ic).Z=Pstruct.Z;
        Lsteady=zeros(1,ZN);
        for z=1:ZN
            Lsteady(z)=Pstruct.dS*mysum(Pstruct.P(:,z,end).*...
                Pstruct.S'.^0.5)*Pstruct.Th(z,end);
            for t=1:TN
                n(z,t)=Pstruct.dS*mysum(Pstruct.P(:,z,t))/Zm;
            end
	end
        nbar=zeros(1,TN);
        for t=1:TN
            nbar(t)=Pstruct.dZ*mysum(n(:,t));
        end
        Zseries(iZ,ic).Lsteady=Lsteady;
        timeseries(iZ,ic).nbar=nbar;
                Sseries(iZ,ic).S=Pstruct.S;
        Sseries(iZ,ic).Pbarend=Pbar(:,end);
        toc
        myfilename=[filebase, '_c' , num2str(ic) , '_Z' , num2str(iZ)]
        save(myfilename,'Pstruct');
        clear Pstruct;
        save([filebase, 'basic'])
    end
end

exit;
