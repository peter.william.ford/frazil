function [ varargout ] = depthaveragednumnew(cn, dT, dS, Tmax, Smax, doplot, Ntype, Thetac)
%UNTITLED2 Summary of this function goes here
%   dPdT = -0.5*Th*dPdS - SP
%   dThdT=1-Th*\int_0^{S_max} S.^0.5 P dS
% with Th(0)=1, P(S,0)=0, P(0,T)=cn
% We use trapezium rule, and first order backward differencing to calculate
% dPdS. This probably doesn't conserve something, but I don't know what.
% I don't think we should be vulnerable to any *new* instabilities from
% introducing the coupling between Th and P, but we must still obey the
% Courant condition for the first equation:
%%% 2*Th*dT<dS
% Steady state has Ths = ???(2/(N*\Gamma(3/4)))^(4/3)Zm^(-7/3), so if 
% Q/N < 1.733, Ths<1

%tic;
SN = 1+floor(Smax/dS);
dS=Smax/(SN-1);
S = dS*(0:(SN-1));

TN = 1+floor(Tmax/dT);
dT = Tmax/(TN-1);
T = dT*(0:(TN-1));

Pold=zeros(1,SN);
Pnew=zeros(1,SN);
Th=zeros(1,TN);
dPdS=zeros(1,SN);
L=zeros(1,TN);
phi=zeros(1,TN);
N=zeros(1,TN);%

%Ntype: 1 => N=Theta; 2=> N=Theta*H(Theta-Thetac)
if(Ntype==1)
    Pnew(1)=cn;
else
    Pnew(1)=0;
end
L(1)=0;
Th(1)=0;

rtdS=dS^0.5;
k=1:(SN-2);
rtSk=(S(2:(SN-1)).^0.5);    
Sk32=(S(2:(SN-1)).^1.5);
for iT=1:(TN-1)
    Pold = Pnew;
    L(iT)=dS*(0.5*Pold(SN)*S(SN)^0.5 + sum(rtSk.*Pold(k+1)));
    phi(iT)=dS*(0.5*Pold(SN)*S(SN)^1.5+sum(Sk32.*Pold(k+1)));
       dPdS(2:SN)=(Pold(2:SN)-Pold(1:(SN-1)))/dS;
       %First order backward differencing
       %dPdS(1) is not needed.
    Pnew(2:SN)=Pold(2:SN) - dT*(0.5*Th(iT)*dPdS(2:SN) + S(2:SN) .* Pold(2:SN));
    Th(iT+1)= Th(iT)+dT*(1 - Th(iT)*L(iT));
    
    if(Ntype == 1)
         %N=Theta
        N(iT+1)=cn*Th(iT+1);
    elseif(Ntype == 2)
        %N=Theta H(Theta - Thetac)
        if(Th(iT+1)>=Thetac)
            N(iT+1)=cn*Th(iT+1);
        else
            N(iT+1)=0;
        end
    elseif(Ntype == 3)
        %N=H(Theta-Thetac)
        if(Th(iT+1)>=Thetac)
            N(iT+1)=cn;
        else
            N(iT+1)=0;
        end
    end
    Pnew(1)=N(iT+1)/Th(iT+1);
end
       
varargout(1)={Th};
varargout(2)={T};
varargout(3)={P};
varargout(4)={S};
varargout(5)={L};
varargout(6)={phi};
end

