function output = Junefull(ct,Zm,dT, Tm, dS, Sm, dZ, freq )
%   We solve the equations:
%   ct*dPdT = S * dPdZ - Th/2 * dPdS + ct*d^2P/dZ^2 
%   dTh/dT=d^2 Th/dZ^2 - Th \int_0^\Smax S^0.5 * P dS
%   Th(Z,0)=P(S,Z,0)=0
%   dTh/dZ(0,T)=-1
%   dTh/dZ(Zm,T)=0; dP/dZ(S,Zm,T)+SP(S,Zm,T)=0

%   P(0,Z,T)=1
%   Courant condition will be something like 
%   dT<2*dS/Th and dT < dZ/Sm and dT < dZ^2
%   Record data at frequency freq (ie output at Tm*freq time points) and
%   plot at that freq
%%Would much rather have adaptive S grid; or at least fixed sigma levels based on the expected Th(Z)?

tic;

SN = 1+floor(Sm/dS)
S = Sm*((1:SN)-1)/(SN-1);
dS = Sm/(SN-1)

TN = 1+floor(Tm/dT)
T = Tm*(0:TN)/(TN-1);
dT = Tm/(TN-1)

ZN = 1+floor(Zm/dZ)
Z = Zm*((1:ZN)-1)/(ZN-1);
dZ = Zm/(ZN-1)


P0 = 1;

freqdN=ceil(TN/Tm/freq)%Output every freqdN timesteps.
Pout=zeros(SN,ZN,floor(TN/freqdN));
Thout=zeros(ZN,floor(TN/freqdN));
Tout=zeros(floor(TN/freqdN),1);


Pold=zeros(SN,ZN);
Pnew=zeros(SN,ZN);
Thold=zeros(1,ZN);
Thnew=zeros(1,ZN);


Pnew(1,1:(ZN-1))=1; 
dPdS=zeros(SN,ZN);
dPdZ=zeros(SN,ZN);
d2ThdZ=zeros(1,ZN);
d2PdZ=zeros(SN,ZN);
I=zeros(1,ZN);

rtSk=S(2:(SN-1)).^0.5;
k=1:(SN-2);

for iT=1:TN
  Pold = Pnew;
  Thold = Thnew;
  
  %First order backward differencing. Which for Z means backwards in
  %-Z
  %But central differencing for the second derivative. Is this
  %inconsistent?
  for iS=2:SN
    dPdS(iS,1:(ZN-1))=(Pold(iS,1:(ZN-1))-Pold(iS-1,1:(ZN-1)))/dS;
    dPdZ(iS,2:(ZN-1))=(Pold(iS,3:ZN)-Pold(iS,2:(ZN-1)))/dZ;
    dPdZ(iS,1)=(Pold(iS,2)-Pold(iS,1))/dZ;
    d2PdZ(iS,2:(ZN-1))=(Pold(iS,3:ZN)-2*Pold(iS,2:(ZN-1))...
			+Pold(iS,1:(ZN-2)))/dZ^2;
    d2PdZ(iS,1)=dPdZ(iS,1)/dZ;
  end
  
  %Central differencing
  for iZ = 2:(ZN-1)
    I(iZ)=dS*(0.5*Pold(SN,iZ)*S(SN)^0.5+sum(rtSk.*Pold(k+1,iZ)'));
    d2ThdZ(iZ)=(Thold(iZ+1)-2*Thold(iZ)+Thold(iZ-1))/dZ^2;
  end
  
  %P(1,1:(ZN-1)) is set by N/Th, so dPdS(1,:) and dPdZ(1,:) are not needed.
  %Setting dP/dZ(S,Zm,T)=0, combined with above, means P(2:SN,(ZN-1))=0 
  %forever.    
  for iS=2:SN
    Pnew(iS,1:(ZN-2))=Pold(iS,1:(ZN-2))+dT/ct*(S(iS)*dPdZ(iS,1:(ZN-2)) - ...
					    0.5*Thold(1:(ZN-2)).*dPdS(iS,1:(ZN-2))+ct*d2PdZ(iS,1:(ZN-2)));
  end
  Thnew(2:(ZN-1))=Thold(2:(ZN-1))+dT*(d2ThdZ(2:(ZN-1))-...
				      Thold(2:(ZN-1)).*I(2:(ZN-1)));
  % P(0,Z,T)=N(Z,T)/Th(Z,T)
  Pnew(1,1:(ZN-1))=NucTh(Thold(1:(ZN-1)),1:(ZN-1),iT,ct);
  % dTh/dZ(0,T)=-1
  Thnew(1)=Thnew(2)+dZ;
  % dTh/dZ(Zm,T)=0;
  Thnew(ZN)=Thnew(ZN-1);
  % dP/dZ(Zm,T)= - SP(Zm.T);
  Pnew(:,ZN)=Pnew(:,ZN-1)-dZ*Pnew(:,ZN-1).*S';
  if(mod(iT,freqdN)==1)
    Pout(:,:,floor(iT/freqdN)+1)=Pnew;
    Thout(:,floor(iT/freqdN)+1)=Thnew;
    Tout(floor(iT/freqdN)+1)=T(iT+1);
    if(mod(iT,10*freqdN)==1)     
      iT
      toc
    end
  end
end
toc
tic;
dimensions=size(Pout);
NR=Pout;
for j = 1:dimensions(2)
  for k=1:dimensions(3)
    NR(:,j,k)=Pout(:,j,k).*S'.^0.5;
  end
end
toc


output.P=Pout;
output.Th=Thout;
output.T=Tout;
output.NR=NR;
output.Z=Z;
output.S=S;
output.dZ=dZ;
output.dS=dS;
output.dT=dT;
output.ct=ct;
output.Tm=Tm;
output.Zm=Zm;
output.Sm=Sm;
output.freq=freq;


function N = NucTh(Th,Z,T,ct)
%Where Z and T are indices in 1:ZN, 1:TN
%Simplest
N=1;

