===============
Full equations
===============


We solve the "full" equations using 'Junefull.m', which is code to integrate these equations given: a pair of parameters (ct, Zm), and maximum and stepsize for time (T), depth (Z) and radius squared (S). It also requires a parameter 'freq' which describes how often to output data; the output files are large.

We want to explore solutions of the equations in a range of parameter space, and so 'createruns.m' is a MATLAB script which helps us set code running to run many copies of 'Junefull' at different parameter values. 'createruns' outputs MATLAB scripts each of which will explore a subregion of the specified parameter space, so that the scripts can each be run on a separate core.

An example set created with the current set of parameters is in the directory 'createrunsoutput'. The files (here called 'julrun' and a number) also calculate various quantities of interest based on the runs, these are described below:

Pbar is \int P dZ
Sbarmax is the value of S at which Pbar*S^0.5 = NRbar is maximised, ie the crystal size with largest depth-averaged number density.
Thbar is \int Th dZ (Th = Theta)
Thbm is the maximum value of Thetabar
Thbs is the eventual (hopefully steady state) value of Thetabar
Lsteady is the steady value of \int P*S^0.5 dS * Theta, ie the ice production rate as a function of depth.
nbar(t) is (\int\int P dS dZ)/Zm ie the depth-averaged total particle number. 


Junefull.m and mysum.m must be in the same directory as the run files (ie julrun1.m) when they are run


=========================
Depth-averaged equations
=========================
We solve equations 5.27-5.28 using 'depthaveragednumnew.m' (in fact, we solve the equations under a slightly different scaling which results in the parameter Z_m c_t appearing in the equations rather than the boundary condition. I will try to look up what the relationship between this scaling and the usual scaling is, it's somewhere in my notes...).
